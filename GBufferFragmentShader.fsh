#version 330 core

layout(location = 0) out ivec2 color;

// Interpolated values from the vertex shaders
in vec2 UV;

void main() {
    color = ivec2(UV.x * 1024, -UV.y * 1024);
}
