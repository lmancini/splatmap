// Include standard headers
#include <stdio.h>
#include <stdlib.h>
#include <vector>

// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <glfw3.h>
GLFWwindow* window;

// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
using namespace glm;

#include <common/shader.hpp>
#include <common/texture.hpp>
#include <common/controls.hpp>
#include <common/objloader.hpp>

GLuint setupRenderBuffer()
{
	// The framebuffer, which regroups 0, 1, or more textures, and 0 or 1 depth buffer.
	GLuint FramebufferName = 0;
	glGenFramebuffers(1, &FramebufferName);
	glBindFramebuffer(GL_FRAMEBUFFER, FramebufferName);

	// The color buffer
	GLuint colorrenderbuffer;
	glGenRenderbuffers(1, &colorrenderbuffer);
	glBindRenderbuffer(GL_RENDERBUFFER, colorrenderbuffer);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_RG32I, 1024, 768);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorrenderbuffer);

	// The depth buffer
	GLuint depthrenderbuffer;
	glGenRenderbuffers(1, &depthrenderbuffer);
	glBindRenderbuffer(GL_RENDERBUFFER, depthrenderbuffer);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, 1024, 768);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthrenderbuffer);

	// Set the list of draw buffers.
	GLenum DrawBuffers[1] = {GL_COLOR_ATTACHMENT0};
	glDrawBuffers(1, DrawBuffers); // "1" is the size of DrawBuffers

	bool success;

	// Always check that our framebuffer is ok
	if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
		success = 0;
	} else {
		success = FramebufferName;
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glBindRenderbuffer(GL_RENDERBUFFER, 0);

	return FramebufferName;
}

void writeToPPM(GLint *buffer)
{
	int backingWidth = 1024;
	int backingHeight = 768;
	static bool done = 1;

	FILE *fp = fopen("dump.ppm", "w");
	fprintf(fp, "P3\n");
	fprintf(fp, "%d %d\n", backingWidth, backingHeight);
	fprintf(fp, "255\n");
	for(int i=0; i < backingHeight; ++i) {
		for (int j=0; j < backingWidth; ++j) {

			int k = ((i * backingWidth) + j) * 2;
			if (!done) {
				printf("%u %u ", buffer[k], buffer[k+1]);
			}
			fprintf(fp, "%u %u %u ",
				(unsigned int)(buffer[k] / 1024.0 * 255),
				(unsigned int)(buffer[k + 1] / 1024.0 * 255),
				0);
		}
		fprintf(fp, "\n");
	}
	fclose(fp);
	done = 1;
}

void dumpRenderBuffer(GLuint FramebufferName, GLint *gbuffer)
{
	const int backingWidth = 1024;
	const int backingHeight = 768;
	const size_t bpp = sizeof(int) * 2;
	static GLint buffer[backingWidth * backingHeight * bpp];

	glBindFramebuffer(GL_FRAMEBUFFER, FramebufferName);
	glReadPixels(0, 0, backingWidth, backingHeight, GL_RG_INTEGER, GL_INT, (GLvoid *)buffer);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	// Flip the buffer on the x-axis after grab: it's upside down on the GPU
	for (int j = 0; j < 768; ++j) {
		for (int i = 0; i < 1024; ++i) {

			int flip_offset = (j * 1024 + i) * 2;
			int buf_offset = ((768 - j - 1) * 1024 + i) * 2;

			gbuffer[flip_offset] = buffer[buf_offset];
			gbuffer[flip_offset + 1] = buffer[buf_offset + 1];
		}
	}
}

int main( void )
{
	// Initialise GLFW
	if( !glfwInit() )
	{
		fprintf( stderr, "Failed to initialize GLFW\n" );
		getchar();
		return -1;
	}

	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	// Open a window and create its OpenGL context
	window = glfwCreateWindow( 1024, 768, "Tutorial 15 - Lightmaps", NULL, NULL);
	if( window == NULL ){
		fprintf( stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n" );
		getchar();
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);

	// Initialize GLEW. It's possible for glewInit to set an error flag
	// GL_INVALID_ENUM. We expect either no error or precisely that error.
	assert(glGetError() == 0);
	glewExperimental = true; // Needed for core profile
	GLenum glewinitres = glewInit();
	GLenum err = glGetError();
	if (glewinitres != GLEW_OK) {
		fprintf(stderr, "Failed to initialize GLEW\n");
		getchar();
		glfwTerminate();
		return -1;
	}
	assert(err == GL_NO_ERROR || err == GL_INVALID_ENUM);
	assert(glGetError() == 0);

	// Ensure we can capture the escape key being pressed below
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
    // Hide the mouse and enable unlimited mouvement
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

    // Set the mouse at the center of the screen
    glfwPollEvents();
    glfwSetCursorPos(window, 1024/2, 768/2);

	// Dark blue background
	glClearColor(0.0f, 0.0f, 0.4f, 0.0f);

	// Enable depth test
	glEnable(GL_DEPTH_TEST);
	// Accept fragment if it closer to the camera than the former one
	glDepthFunc(GL_LESS);

	// Cull triangles which normal is not towards the camera
	glEnable(GL_CULL_FACE);

	GLuint VertexArrayID;
	glGenVertexArrays(1, &VertexArrayID);
	glBindVertexArray(VertexArrayID);

	// Create and compile our GLSL program from the shaders
	GLuint programID = LoadShaders( "TransformVertexShader.vertexshader", "TextureFragmentShaderLOD.fragmentshader" );
	GLuint gbprogramID = LoadShaders("GBufferVertexShader.vsh", "GBufferFragmentShader.fsh");

	// Get a handle for our "MVP" uniform
	GLuint MatrixID = glGetUniformLocation(programID, "MVP");

	// Load the texture
	GLuint Texture = loadBMP_custom("lightmap.bmp");

	// Get a handle for our "myTextureSampler" uniform
	GLuint TextureID  = glGetUniformLocation(programID, "myTextureSampler");

	// Read our .obj file
	std::vector<glm::vec3> vertices;
	std::vector<glm::vec2> uvs;
	std::vector<glm::vec3> normals; // Won't be used at the moment.
	bool res = loadOBJ("room.obj", vertices, uvs, normals);

	// Load it into a VBO

	GLuint vertexbuffer;
	glGenBuffers(1, &vertexbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3), &vertices[0], GL_STATIC_DRAW);

	GLuint uvbuffer;
	glGenBuffers(1, &uvbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
	glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(glm::vec2), &uvs[0], GL_STATIC_DRAW);

	GLuint FramebufferName = setupRenderBuffer();
	if (FramebufferName == 0) {
		fprintf(stderr, "setting up renderbuffer failed\n");
		return -1;
	}

	printf("Press space to splat!\n");

	GLubyte *splatmap = (GLubyte*)malloc(1024 * 1024 * 3);
	GLint *gbuffer = (GLint *)malloc(1024 * 768 * sizeof(int) * 2);

	do {
		// Compute the MVP matrix from keyboard and mouse input
		computeMatricesFromInputs();
		glm::mat4 ProjectionMatrix = getProjectionMatrix();
		glm::mat4 ViewMatrix = getViewMatrix();
		glm::mat4 ModelMatrix = glm::mat4(1.0);
		glm::mat4 MVP = ProjectionMatrix * ViewMatrix * ModelMatrix;

		// Bind our texture in Texture Unit 0
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, Texture);

		// 1rst attribute buffer : vertices
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
		glVertexAttribPointer(
			0,                  // attribute
			3,                  // size
			GL_FLOAT,           // type
			GL_FALSE,           // normalized?
			0,                  // stride
			(void*)0            // array buffer offset
		);

		// 2nd attribute buffer : UVs
		glEnableVertexAttribArray(1);
		glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
		glVertexAttribPointer(
			1,                                // attribute
			2,                                // size
			GL_FLOAT,                         // type
			GL_FALSE,                         // normalized?
			0,                                // stride
			(void*)0                          // array buffer offset
		);

		// Space pressed, calculate gbuffer and update splatmap
		if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS) {
			// Write the gbuffer
			// (we'll need to change MVP with the splat coords)
			glUseProgram(gbprogramID);
			glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
			glBindFramebuffer(GL_FRAMEBUFFER, FramebufferName);
			glViewport(0,0,1024,768);
			glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
			glEnable(GL_DEPTH_TEST);
			glDepthFunc(GL_LESS);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			glDrawArrays(GL_TRIANGLES, 0, vertices.size());
			glBindFramebuffer(GL_FRAMEBUFFER, 0);

			// dump the gbuffer
			dumpRenderBuffer(FramebufferName, gbuffer);
#ifdef DEBUG
			writeToPPM(gbuffer);
#endif

			// dump the splatmap
			glGetTexImage(GL_TEXTURE_2D, 0, GL_RGB, GL_UNSIGNED_BYTE, splatmap);

			int hw = 1024 / 2;
			int hh = 768 / 2;

			for(int ib=hw - 20; ib < hw + 20; ++ib) {
				for(int jb=hh - 20; jb < hh + 20; ++jb) {
					// Sample UVs from gbuffer
					int u = gbuffer[(jb * 1024 + ib) * 2];
					int v = gbuffer[(jb * 1024 + ib) * 2 + 1];

					if (u == 0 && v == 0)
						continue;

					// Flip the V coordinate
					v = 1024 - v;

					// Write to those UV in the splatmap
					splatmap[(v * 1024 + u) * 3] = 255;
					splatmap[(v * 1024 + u) * 3 + 1] = 0;
					splatmap[(v * 1024 + u) * 3 + 2] = 0;
				}
			}

			// Reupload the splatmap
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 1024, 1024, 0, GL_RGB, GL_UNSIGNED_BYTE, splatmap);
		}

		// Clear the screen
		glClearColor(0.0f, 0.0f, 0.4f, 0.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// Use our shader
		glUseProgram(programID);

		// Send our transformation to the currently bound shader,
		// in the "MVP" uniform
		glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);

		// Set our "myTextureSampler" sampler to user Texture Unit 0
		glUniform1i(TextureID, 0);

		// Draw the triangles !
		glDrawArrays(GL_TRIANGLES, 0, vertices.size() );

		glUseProgram(0);

		glDisableVertexAttribArray(1);
		glDisableVertexAttribArray(0);

		// Swap buffers
		glfwSwapBuffers(window);
		glfwPollEvents();

	} // Check if the ESC key was pressed or the window was closed
	while( glfwGetKey(window, GLFW_KEY_ESCAPE ) != GLFW_PRESS &&
		   glfwWindowShouldClose(window) == 0 );

	free(gbuffer);
	free(splatmap);

	// Cleanup VBO and shader
	glDeleteBuffers(1, &vertexbuffer);
	glDeleteBuffers(1, &uvbuffer);
	glDeleteProgram(programID);
	glDeleteTextures(1, &Texture);
	glDeleteVertexArrays(1, &VertexArrayID);

	// Close OpenGL window and terminate GLFW
	glfwTerminate();

	return 0;
}

